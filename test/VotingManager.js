const assertRevert = require('./helpers/assertRevert');
const createVoting = require('./helpers/createVoting');

require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const VotingManager = artifacts.require('VotingManager');
const VotingManagerTest = artifacts.require('VotingManagerTest');

contract('VotingManager', ([owner1, owner2, owner3, owner4, owner5, 
                            owner6, owner7, owner8, owner9, owner10]) => {
    const oneDay = 60*60*24;
    const oneHour = 60*60;
    let dateNow;
    let vman;
    
    before('Setup', async () => {
        vman = await VotingManager.new(100, {from: owner1});        
    });

    beforeEach(async () => {
        dateNow = (await vman.getNow.call()).toNumber()+1;// get the latest block timestamp
    });

    describe('Participants registration', () => {

        it('Should fail if passed uknown voting id', async () => {
            await assertRevert(vman.methods['register(bytes32)'](web3.utils.asciiToHex('unknown'), {from: owner2}), 'UNKNOWN_VOTING');
        });

        it('Should register new participant and emit a proper event', async () => {
            const voting = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + oneHour,
                    dateNow + oneHour + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            );
            const result = await vman.methods['register(bytes32)'](voting.id, {from: owner1});
            const events = result.logs.filter(l => l.event === 'ParticipantRegistered');
            (events.length).should.equal(1);
            (events[0].args.participant).should.equal(owner1);
            (events[0].args.votingId).should.equal(voting.id);
        });

        it('Should fail if participant already has been registered', async () => {
            const voting = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + oneHour,
                    dateNow + oneHour + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            );
            await vman.methods['register(bytes32)'](voting.id, {from: owner1});
            await assertRevert(vman.methods['register(bytes32)'](voting.id, {from: owner1}), 'ALREADY_REGISTERED');
        });

        it('Should fail if registration finished (voting already started)', async () => {
            const voting = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + 1,
                    dateNow + 1 + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            );
            await new Promise((resolve, reject) => {
                setTimeout(() => {
                    assertRevert(vman.methods['register(bytes32)'](voting.id, {from: owner1}), 'REGISTRATION_FINISHED')
                        .then(resolve)
                        .catch(reject);
                }, 2500);
            });
        });

        describe('Detecting is participant is registered', () => {

            it('#isRegistered should return true if participant is already registered', async () => {
                const voting = await createVoting(
                    vman,
                    [
                        'New vote', 
                        dateNow + oneHour,
                        dateNow + oneHour + oneDay,
                        ['one', 'two', 'three']
                    ],
                    owner1
                );
                await vman.methods['register(bytes32)'](voting.id, {from: owner1});
                const registered = await vman.isRegistered(owner1, voting.id);
                (registered).should.be.true;
            });

            it('#isRegistered should fail if called from owner with zero address', async () => {
                const vmanTest = await VotingManagerTest.new(100, {from: owner1});
                await assertRevert(vmanTest.methods['testIsRegisteredWithWrongAddress()']({from: owner1}), 
                    process.env.SOLIDITY_COVERAGE ? false : 'INVALID_ADDRESS');
            });
        });
    });

    describe('Voting creation', () => {
        

        it('Should fail if passed wrong start date',  async () => {
            await assertRevert(createVoting(
                vman,
                [
                    'New vote', 
                    dateNow - oneHour,
                    dateNow + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            ), 'WRONG_START_DATE');
        });

        it('Should fail if passed wrong stop date',  async () => {
            await assertRevert(createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + oneHour,
                    dateNow - oneHour,
                    ['one', 'two', 'three']
                ],
                owner1
            ), 'WRONG_STOP_DATE');
        });

        it('Should fail if stop date less when start date',  async () => {
            await assertRevert(createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + oneHour*3,
                    dateNow + oneHour,
                    ['one', 'two', 'three']
                ],
                owner1
            ), 'WRONG_START_STOP_CONFIG');
        });

        it('Should create a new voting and emit a proper event', async () => {
            const result = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + oneHour,
                    dateNow + oneHour + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            );
            (result.id).should.be.a('string');
            (result.createdBy).should.equal(owner1);
        });        
    });

    describe('Voting finish detection with #isFinished', () => {
        
        it('Should fail if passed unknown votings Id', async () => {
            await assertRevert(vman.isFinished(web3.utils.asciiToHex('unknown')), 'UNKNOWN_VOTING');
        });

        it('Should return false for not finished voting', async () => {
            const result = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + oneHour,
                    dateNow + oneHour + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            );

            (await vman.isFinished.call(result.id)).should.not.be.true;
        });

        it('Should return true for finished voting', async () => {
            const result = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow,
                    dateNow + 1,
                    ['one', 'two', 'three']
                ],
                owner1
            );

            const isFinished = await new Promise((resolve, reject) => 
                setTimeout(() => {
                    vman.isFinished.call(result.id).then(resolve).catch(reject);
                }, 2100));
            (isFinished).should.be.true;
        });
    });

    describe('Getting the voting details', () => {

        it('Should fail if passed unknown voting Id', async () => {
            await assertRevert(vman.details(web3.utils.asciiToHex('unknown')), 'UNKNOWN_VOTING');
        });

        it('Should return voting details', async () => {
            const config = {
                name: 'New name',
                start: dateNow + oneHour,
                stop: dateNow + oneHour + oneDay,
                options: ['one', 'two', 'three']
            };
            const result = await createVoting(
                vman,
                [
                    config.name, 
                    config.start,
                    config.stop,
                    config.options
                ],
                owner1
            );

            const details = await vman.details(result.id);

            (web3.utils.hexToUtf8(details.name)).should.equal(config.name);
            (details.start).should.eq.BN(config.start);
            (details.stop).should.eq.BN(config.stop);
            (details.options).should.be.an('Array');
            (details.options.length).should.equal(config.options.length);
        });
    });

    describe('Getting of votings ids with #getVotingsIds', () => {

    });

    describe('Voting process with #vote', () => {
        let votingId;

        beforeEach(async () => {
            const dateNow = (await vman.getNow.call()).toNumber()+1;
            const result = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + 1,
                    dateNow + 1 + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            );

            votingId = result.id;

            await vman.methods['register(bytes32)'](votingId, {from: owner1});
            await vman.methods['register(bytes32)'](votingId, {from: owner2});
            
            // wait for the voting starts
            await new Promise((resolve) => setTimeout(resolve, 2000));
        });

        it('should fail if passed unknown votingId', async () => {
            await assertRevert(vman.methods['vote(bytes32,uint256,uint256)'](web3.utils.asciiToHex('unknown'), 0, 20), 'UNKNOWN_VOTING');
        });

        it('should fail if voting not started', async () => {
            const result = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + oneHour,
                    dateNow + oneHour + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            );
            await vman.methods['register(bytes32)'](result.id, {from: owner1});
            await assertRevert(vman.methods['vote(bytes32,uint256,uint256)'](result.id, 0, 20), 'VOTING_NOT_STARTED');
        });

        it('should fail if participant not registered for the voting', async () => {
            await assertRevert(vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 20, {from: owner3}), 'NOT_REGISTERED');
        });

        it('should make a vote', async () => {
            const result = await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 20, {from: owner1});
            const voicesLeft = await vman.voicesLeft.call();
            (voicesLeft).should.eq.BN(80);
            const events = result.logs.filter(l => l.event === 'Voted');
            (events.length).should.equal(1);
            (events[0].args.participant).should.equal(owner1);
            (events[0].args.votingId).should.equal(votingId);
            (events[0].args.option).should.eq.BN(0);
            (events[0].args.votes).should.eq.BN(20);
        });

        it('should fail if participant already have voted', async () => {
            await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 10, {from: owner2});
            await assertRevert(vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 10, {from: owner2}), 'ALREADY_VOTED');
        });

        it('should fail if passed worng max votes value', async () => {
            const vmanTest = await VotingManagerTest.new(100, {from: owner1});
            const result = await createVoting(
                vmanTest,
                [
                    'New vote', 
                    dateNow + oneHour,
                    dateNow + oneHour + oneDay,
                    ['one', 'two', 'three']
                ],
                owner1
            );
            await vmanTest.methods['register(bytes32)'](result.id, {from: owner1});
            await assertRevert(vmanTest.methods['testMaxVotesDuringTheVote(bytes32,uint256,uint256)'](result.id, 0, 20, {from: owner1}), 
                    process.env.SOLIDITY_COVERAGE ? false : 'VOTING_NOT_STARTED');
        });
    });

    describe('Getting of the votings result with #result', () => {
        let votingId;

        beforeEach(async () => {
            const dateNow = (await vman.getNow.call()).toNumber()+1;
            const result = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + 1,
                    dateNow + 1 + oneDay,
                    ['one', 'two', 'three',]
                ],
                owner1
            );

            votingId = result.id;

            await vman.methods['register(bytes32)'](votingId, {from: owner1});
            await vman.methods['register(bytes32)'](votingId, {from: owner2});
            await vman.methods['register(bytes32)'](votingId, {from: owner3});
            await vman.methods['register(bytes32)'](votingId, {from: owner4});
            await vman.methods['register(bytes32)'](votingId, {from: owner5});
            
            // wait for the voting starts
            await new Promise((resolve) => setTimeout(resolve, 2000));
        });

        it('should return result of the voting', async () => {
            await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 50, {from: owner1});
            await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 1, 90, {from: owner2});
            await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 10, {from: owner3});
            await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 1, 20, {from: owner4});
            await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 5, {from: owner5});
            const result1 = await vman.result.call(votingId, 0);
            const result2 = await vman.result.call(votingId, 1);
            (result1).should.eq.BN(12);//sqrt(50)+sqrt(10)+sqrt(5)
            (result2).should.eq.BN(13);//sqrt(90)+sqrt(20)
        });
    });

    describe('Getting of the voting winner', () => {
        let votingId;

        it('Should fail if passed wrong voting Id', async () => {
            await assertRevert(vman.winner.call(web3.utils.asciiToHex('unknown')), 'UNKNOWN_VOTING');
        });

        it('Should return a winner', async () => {
            const dateNow = (await vman.getNow.call()).toNumber()+1;
            const result = await createVoting(
                vman,
                [
                    'New vote', 
                    dateNow + 1,
                    dateNow + 4,
                    ['one', 'two', 'three']
                ],
                owner1
            );

            votingId = result.id;

            await vman.methods['register(bytes32)'](votingId, {from: owner6});
            await vman.methods['register(bytes32)'](votingId, {from: owner7});
            await vman.methods['register(bytes32)'](votingId, {from: owner8});
            await vman.methods['register(bytes32)'](votingId, {from: owner9});
            await vman.methods['register(bytes32)'](votingId, {from: owner10});

            await new Promise((resolve, reject) => {

                // waiting for voting starts
                setTimeout(async () => {
                    try {
                        await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 50, {from: owner6});
                        await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 1, 90, {from: owner7});
                        await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 10, {from: owner8});
                        await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 1, 20, {from: owner9});
                        await vman.methods['vote(bytes32,uint256,uint256)'](votingId, 0, 5, {from: owner10});
                        resolve();
                    } catch(e) {
                        reject(e);
                    }
                }, 2000);
            });

            await assertRevert(vman.winner.call(votingId), 'VOTING_NOT_FINISHED');

            const winner = await new Promise((resolve, reject) => {

                // waiting for voting finished
                setTimeout(() => {
                    vman.winner.call(votingId).then(resolve).catch(reject);
                }, 5100);
            });

            //sqrt(50)+sqrt(10)+sqrt(5) = 12 
            //sqrt(90)+sqrt(20) = 13 --- winner index 1

            (winner).should.eq.BN(1);
        });
    });
});
