module.exports = async (contract, options = [], owner) => {
    options[0] = web3.utils.asciiToHex(options[0]);
    options[3] = options[3].map(i => web3.utils.asciiToHex(i));
    const result = await contract.methods['create(bytes32,uint256,uint256,bytes32[])'].apply(undefined, [...options, ...[{from: owner}]]);
    const events = result.logs.filter(l => l.event === 'VotingCreated');
    return events[0].args;
};
