const assertRevert = require('./helpers/assertRevert');

require('chai').should();

const Qvote = artifacts.require('Qvote');

contract('Qvote', ([owner1, owner2, owner3]) => {
    let qvote;

    before('Setup', async () => {
        qvote = await Qvote.deployed();
    });

    describe('Version getter', () => {

        it('Should return a string with contract number', async () => {
            const version = await qvote.version.call({from: owner1});
            (version).should.be.a('string', 'version is not a string');
        });
    });

    describe('Contract constructor', () => {

        it('Should fail if passed wrong credit value',  async () => {
            await assertRevert(Qvote.new(0, {from: owner1}), 'WRONG_CREDIT_VALUE');
        });
    });
});
