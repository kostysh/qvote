# Dapp for Qvote smart contract

Веб приложение является **упрощенным примером** того как можно использовать 
смарт контракт предназначенный для голосования по квардратичному принципу.

## Основные принципы
- Голосования может создавать любой аккаунт в любом количестве
- Для выполнения операций изменяющих состояние смарт контракта аккаунт пользователя должен иметь положительный баланс (каждая операция использует небольшое количество газа) 
- Любой аккаунт может участвовать в неограниченном количестве голосований
- Предельное количество голосов, которые отдельный аккаунт может использовать ограничено и является константой, которая задается при создании смарт контракта и, впоследствии, не может быть изменено (в текущей версии контракта это число равняется 100)
- Каждое голосвание может иметь неограниченное колчество вариантов выбора
- Длина названия голосования и длина наименования каждого из вариантов ограничена 32-мя символами (сделано специально, для того, чтобы операции с контрактом были дешевле)
- Для указания даты начала и конца голосования используются только месяц, день и год (ограничение касается только интерфейса приложения, сам смарт контракт принимает значения в виде unix timestamp, что позволяет указывать любое время)
- Для участия в голосовании необходимо зарегистрировать аккаунт. Незарегистрированные аккаунты не могут принимать участие в голосовании..
- Зарегистрировать аккаунт в голосовании можно только до того как оно начнется, если голосование началось, то регистрация закрывается
- Каждый аккаунт может проголосовать в одном отдельном голосовании только один раз
- Голосуя каждый аккаунт может указать необходимое количество голосов, которое не может превышать максимальное количество голосов (см. выше) 
- После каждого голосования оставшееся (доступное) количество голосов пересчитывается и отображается в заголовке приложения 
- После наступления даты окончания голосования новые голоса не принимаются
- В соответствии с правилами учета голосов итоговые значения по каждому варианту представляют собой сумму квадратичных корней всех голосов. Например, если в процессе голосования пользователь указал число голосов "10", то в общей сумме будет зачтено 3 голоса (целая часть от квадратного корня из 10). Победившим считается вариант набравший максимальную сумму.  

## Использование плагина Metamask
- Для взаимодействия приложения со смарт контрактом необходимо, чтобы в браузере был установлен и настроен плагин [Metamask](https://metamask.io/)
- Во время первого запука приложения выполняется инициализация во время которой плагин запросит подтверждение от пользователя. Для функционирования приложения необходимо дать плагину разрешение на подключение.
- Аккаунт, который выбран в качестве текущего в плагине будет использован для подписывания транзакций. Если переключиться на другой аккаунт - то список голосований будет перезагружен и новый текущий адрес будет использован автоматически
- При выполнении операций со смарт контрактом плагин будет запрашивать подтверждение (будет появляться поп-ап окно в котором есть кнопки "подтвердить" и "отклонить")
- Все транзакции выполненные в процессе взаимодействия со смарт контрактом отображаются в интерфейсе плагина и, при необходимости, там можно получить прямую ссылку на просмотр транзакции в блокчейн эксплорере etherscan

## Индикатор статуса подключения  
![App header](./images/header.png)  
- "Зеленый" цвет индикатора свидетельствует о том, что плагин Metamask настроен и подключен к сети
- "Красный" цвет свидетельствует о наличии ошибок. Текст ошибки будет выведен на главной странице

## Счетчик оставшихся голосов
- В данной версии смарт контракта максимально доступное количество голосов постоянно и ограничено числом 100
- После каждого голосования счетчик будет отображать оставшееся количество голосов

## Создание нового голосования
![New voting](./images/create.png)  
Для создания голосования нужно заполнить следующие поля:
- Название голосования: текст, 32 символа
- Дата начала: месяц-день-год, не ранее следующего за текущим дня, если указать текущий день, то возможности зарегистрировать не будет (дополнительных проверок в приложении нет)
- Дата конца голосования: месяц-день-год, не ранее одного дня следующего за датой начала голосования
- Варианты голосования (любое число, новые добавляются кнопкой "добавить"): текст, 32 символа

## Список голосований
![Votings list](./images/votings-noreg-novote.png)  
- Список голосований располагается на главной странице. В заголовке указывается название и даты начала и конца
- Каждое голосование "раскрывается" по клику на заголовок
- Таблица голования отображает варианты голосования и результаты, а также кнопки для регистрации и сохранения выбора варианта

## Регистрация участника голосования  
![New voting](./images/register.png)  
- До момента начала голосования в верхней части таблицы отображается кнопка регистрации
- После нажатия на кнопку голосования выполняется соответствующая транзакция, которую необходимо подтвердить в окне плагина
- После того как транзакция выполнена, регистрация засчитывается и кнопка регистрации больше не отображается в интерфейсе голосования

## Голосование  
![Voting](./images/voting.png) | ![Voting](./images/metamask-confirm.png) 
--- | ---
- Кнопка голосования становится доступной только после того как наступит момент начала голосования 
- После нажатия на кнопку голосования выполняется соответствующая транзакция, которую необходимо подтвердить в окне плагина
- После того как транзакция выполнена, голосование засчитывается 

![Voting](./images/voting-done.png)

