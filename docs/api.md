# Qvote smart contract API
Detailed information about functions please see in the source codes. 
Smart contract rules are mostly placed [in the library](../contracts/libraries/VotingLib.sol).

## Active methods

- `register(bytes32 votingId)`: Registration of a new participant
- `create(bytes32 name, uint256 start, uint256 stop, bytes32[] calldate options)`: Creating of the new voting
- `vote(bytes32 votingId, uint256 option, uint256 votes)`: Make a vote

## View methods

- `isRegistered(address owner, bytes32 votingId)`: Check is participant is already registered, return `bool` value
- `isStarted(bytes32 votingId)`: Is voting is started, return `bool` value
- `isFinished(bytes32 votingId)`: Is voting is finished, return `bool` value
- `details(bytes32 votingId)`: Get the voting details, return `bool` value
- `votingsIds()`: Get all votings ids, return `array` of votings ids
- `voicesLeft()`: Get the number of voices left, return `number`(BN) value
- `result(bytes32 votingId, uint256 optionIndex)`: Get the voting result, return `array` of votings results (in order of options)
- `winner(bytes32 votingId)`: Get winner of the voting, return `number` (BN), an index of voting option

## Events
- `ParticipantRegistered(address participant, bytes32 votingId)`: emitted when new participant is registered
- `VotingCreated(bytes32 id, address createdBy)`: emitted when new voting is created
- `Voted(address participant, bytes32 votingId, uint256 option, uint256 votes)`: emitted when participant is making a vote