pragma solidity 0.5.0;

import "./managers/VotingManager.sol";


/**
 * @title Qvote smart contract
 * @dev A smart contract that implements a quadratic voting mechanism
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract Qvote is VotingManager {
    
    /// @dev Contract version
    bytes32 public constant version = "0.0.2";

    /**
     * @dev Qvote constructor
     * @param creditValue A maximum number of available votes
     */
    constructor(
        uint256 creditValue
    ) public VotingManager(creditValue) {}
}
