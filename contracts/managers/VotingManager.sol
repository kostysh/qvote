pragma solidity 0.5.0;

import "./IVotingManager.sol";
import "../libraries/VotingLib.sol";


/**
 * @title VotingManager
 * @dev This contract represents votings management logic
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract VotingManager is IVotingManager {

    /// @dev Partisipants storage management library
    using VotingLib for VotingLib.VotingStorage;

    /// @dev A maximum number of available votes for each participant
    uint256 public creditValue;

    /// @dev Votings storage
    VotingLib.VotingStorage internal votings;

    /**
     * @dev VotingManager constructor
     * @param _creditValue A maximum number of available votes
     */
    constructor(
        uint256 _creditValue
    ) public {
        require(_creditValue > 1, "WRONG_CREDIT_VALUE");
        creditValue = _creditValue;
    }

    /**
     * @dev Registration of new participant
     * @param votingId Unique voting Id
     */
    function register(bytes32 votingId) external {
        votings.register(msg.sender, votingId);
        emit ParticipantRegistered(msg.sender, votingId);
    }
    
    /**
     * @dev Creating of the new voting
     * @param name Voting name
     * @param start Voting start date
     * @param stop Voting stop date
     * @param options Voting variants
     */
    function create(
        bytes32 name, 
        uint256 start, 
        uint256 stop,
        bytes32[] calldata options
    ) external {
        VotingLib.Voting memory voting = votings.createVoting(name, start, stop, options);
        emit VotingCreated(voting.id, msg.sender);
    }

    /**
     * @dev Make a vote
     * @param votingId Unique voting Id
     * @param option Voting option index
     * @param votes Number of votes to use
     */
    function vote(
        bytes32 votingId, 
        uint256 option, 
        uint256 votes
    ) external {
        votings.vote(votingId, msg.sender, option, votes, creditValue);
        emit Voted(msg.sender, votingId, option, votes);
    }

    /**
     * @dev Is voting is started
     * @param votingId Unique voting Id
     * @return bool
     */
    function isStarted(bytes32 votingId) external view returns (bool) {
        return votings.isStarted(votingId);
    }

    /**
     * @dev Is voting is finished
     * @param votingId Unique voting Id
     * @return bool
     */
    function isFinished(bytes32 votingId) external view returns (bool) {
        return votings.isFinished(votingId);
    }

    /**
     * @dev Get the voting result
     * @param votingId Unique voting Id
     * @param optionIndex Option index
     */
    function result(bytes32 votingId, uint256 optionIndex) external view returns (uint256) {
        return votings.result(votingId, optionIndex);         
    }

    /**
     * @dev Get voting details
     * @param votingId Unique voting Id
     * @return details of the voting
     */
    function details(bytes32 votingId) external view returns (
        bytes32 name, 
        uint256 start, 
        uint256 stop, 
        bytes32[] memory options
    ) {
        VotingLib.Voting memory voting = votings.details(votingId);
        name = voting.name;
        start = voting.start;
        stop = voting.stop;
        options = voting.options;
    }

    /**
     * @dev Get winner of the voting
     * @param votingId Unique voting Id
     * @return index of the winner
     */
    function winner(bytes32 votingId) external view returns (uint256) {
        return votings.winner(votingId);
    }

    /**
     * @dev Get the number of voices left
     * @return uint256 number of voices left
     */
    function voicesLeft() external view returns (uint256) {
        return votings.voicesLeft(msg.sender, creditValue);
    }

    /**
     * @dev Check is participant is already registered
     * @param owner Participant address
     * @param votingId Unique voting Id
     * @return bool
     */
    function isRegistered(address owner, bytes32 votingId) external view returns (bool) {
        return votings.isRegistered(owner, votingId);        
    }

    /**
     * @dev Get votings list
     * @return bytes32[]
     */
    function votingsIds() external view returns (bytes32[] memory) {
        return votings.votingsIds;
    }

    /**
     * @dev Get current block time (just for testing features)
     * @return uint256
     */
    function getNow() public view returns (uint256) {
        return now;
    }
}
