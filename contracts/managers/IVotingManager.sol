pragma solidity 0.5.0;


/**
 * @title IVotingManager
 * @dev Interface to VotingManager contract
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
interface IVotingManager {

    /**
     * @dev This event will be emitted when new participant is registered
     * @param participant The participant address
     * @param votingId Unique voting Id
     */
    event ParticipantRegistered(
        address participant, 
        bytes32 votingId
    );

    /**
     * @dev This event will be emitted when new voting is created
     * @param id Unique voting id
     * @param createdBy Voting creator address
     */
    event VotingCreated(
        bytes32 id,
        address createdBy
    );

    /**
     * @dev This event will be emitted when participant is making a vote
     * @param participant The participant address
     * @param votingId Unique voting Id
     * @param votes Number of used votes
     */
    event Voted(
        address participant, 
        bytes32 votingId,
        uint256 option,
        uint256 votes
    );

    function register(bytes32 votingId) external;
    function create(bytes32 name, uint256 start, uint256 stop, bytes32[] calldata options) external;
    function vote(bytes32 votingId, uint256 option, uint256 votes) external;
    function votingsIds() external view returns (bytes32[] memory);
    function isRegistered(address owner, bytes32 votingId) external view returns (bool);
    function isStarted(bytes32 votingId) external view returns (bool);
    function isFinished(bytes32 votingId) external view returns (bool);
    function result(bytes32 votingId, uint256 optionIndex) external view returns (uint256);
    function details(bytes32 votingId) external view returns (bytes32 name, uint256 start, uint256 stop, bytes32[] memory options);
    function winner(bytes32 votingId) external view returns (uint256);
    function voicesLeft() external view returns (uint256);
}
