pragma solidity 0.5.0;


/**
 * @title VotingLib
 * @dev VotingLib libraty represents votings storage management logic
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
library VotingLib {

    /// @Vote structure
    struct Vote {
        bytes32 id;// Unique voting Id
        uint256 option;// Voting option Id (index number)
        address participant;// Voting participant
        uint256 count;// Number of used votes
    }

    /// @dev Voting data structure
    struct Voting {
        bytes32 id;// Unique voting Id
        bytes32 name;// Voting name
        uint256 start;// Voting start date
        uint256 stop;// Voting end date
        uint256 count;// Voting participants count
        bytes32[] options;// Voting options
        address[] participantsAddresses;// Array with all participants addresses
        mapping(address => bool) participants;// owner => bool 
        mapping(address => Vote) votes;// Voting votes, participant address => Vote
    }

    /// @dev Voting storage
    struct VotingStorage {
        mapping(bytes32 => Voting) votings;// votingId => Voting obj
        bytes32[] votingsIds;//Array with votings Ids
        mapping (bytes32 => bool) hashes;// unique hashes list
        bytes32 lastHash;// last generated hash
    }

    /**
     * @dev Check is participant is already registered
     * @param store Votings storage
     * @param owner Participant address
     * @param votingId Unique voting Id
     * @return bool
     */
    function isRegistered(
        VotingStorage storage store,
        address owner, 
        bytes32 votingId
    ) internal view returns (bool) {
        require(owner != address(0), "INVALID_ADDRESS");
        return store.votings[votingId].participants[owner];
    }

    /**
     * @dev Registration of new participant
     * @param store Votings storage
     * @param owner Participant address
     * @param votingId Unique voting Id
     */
    function register(
        VotingStorage storage store,
        address owner, 
        bytes32 votingId
    ) internal {
        require(store.votings[votingId].id == votingId, "UNKNOWN_VOTING");
        require(now <= store.votings[votingId].start, "REGISTRATION_FINISHED");
        require(!isRegistered(store, owner, votingId), "ALREADY_REGISTERED");
        store.votings[votingId].participants[owner] = true;
        store.votings[votingId].count += 1;
    }

    /**
     * @dev Creating of the new voting
     * @param store Votings storage
     * @param name Voting name
     * @param start Voting start date
     * @param stop Voting stop date
     * @param options Voting options
     * @return Voting object
     */
    function createVoting(
        VotingStorage storage store,
        bytes32 name,
        uint256 start,
        uint256 stop,
        bytes32[] memory options
    ) internal returns (Voting storage) {
        require(now < start, "WRONG_START_DATE");
        require(now < stop, "WRONG_STOP_DATE");
        require(start < stop, "WRONG_START_STOP_CONFIG");
        require(options.length > 1, "WRONG_VOTING_OPTIONS_COUNT");
        bytes32 votingId = getUniqueHash(store);
        store.votings[votingId] = Voting({
            id: votingId,
            name: name,
            start: start,
            stop: stop,
            count: 0,
            options: options,
            participantsAddresses: new address[](0)
        });
        store.votingsIds.push(votingId);
        return store.votings[votingId];
    }

    /**
     * @dev Make a vote
     * @param store Votings storage
     * @param votingId Unique voting Id
     * @param participant Voting participant address
     * @param option Voting variant index
     * @param votes Number of used votes
     * @param maxVotes Maximum number of votes available for the participant
     */
    function vote(
        VotingStorage storage store,
        bytes32 votingId,
        address participant,
        uint256 option,
        uint256 votes,
        uint256 maxVotes
    ) internal {
        require(store.votings[votingId].id == votingId, "UNKNOWN_VOTING");
        require(isRegistered(store, participant, votingId), "NOT_REGISTERED");
        require(now >= store.votings[votingId].start, "VOTING_NOT_STARTED");
        require(now <= store.votings[votingId].stop, "VOTING_FINISHED");
        require(store.votings[votingId].votes[participant].participant != participant, "ALREADY_VOTED");
        require(maxVotes >= 1, "WRONG_MAX_VOTES");

        uint256 totalVotes = votes;

        // Current (simple) implementation of the voting uses fixed value of maximally available number of votes.
        // The real-life case requires time period depended calculation of this number.
        for (uint256 i = 0; i < store.votingsIds.length; i++) {

            if (store.votings[store.votingsIds[i]].votes[participant].participant != address(0)) {
                totalVotes += store.votings[store.votingsIds[i]].votes[participant].count;
            }
        }

        require(totalVotes <= maxVotes, "INSUFFICIENT_VOTES");

        store.votings[votingId].participantsAddresses.push(participant);
        store.votings[votingId].votes[participant] = Vote({
            id: votingId,
            option: option,
            participant: participant,
            count: votes
        });
    }

    /**
     * @dev Get voting details
     * @param store Votings storage
     * @param votingId Unique voting Id
     * @return Voting object
     */
    function details(VotingStorage storage store, bytes32 votingId) internal view returns (Voting storage) {
        require(store.votings[votingId].id == votingId, "UNKNOWN_VOTING");
        return store.votings[votingId];
    }

    /**
     * @dev Get winner of the voting
     * @param store Votings storage
     * @param votingId Unique voting Id
     * @return index of the winner
     */
    function winner(VotingStorage storage store, bytes32 votingId) internal view returns (uint256) {
        require(store.votings[votingId].id == votingId, "UNKNOWN_VOTING");
        require(now > store.votings[votingId].stop, "VOTING_NOT_FINISHED");

        uint256 currentResult;
        uint256 winnerValue = 0;
        uint256 winnerIndex = 0;

        for (uint256 i = 0; i < store.votings[votingId].options.length; i++) {
            currentResult = result(store, votingId, i);

            if (currentResult > winnerValue) {
                winnerValue = currentResult;
                winnerIndex = i;
            }
        }

        return winnerIndex;
    }

    /**
     * @dev Is voting is started
     * @param store Votings storage
     * @param votingId Unique voting Id
     * @return bool
     */
    function isStarted(VotingStorage storage store, bytes32 votingId) internal view returns (bool) {
        require(store.votings[votingId].id == votingId, "UNKNOWN_VOTING");
        return now > store.votings[votingId].start;
    }

    /**
     * @dev Is voting is finished
     * @param store Votings storage
     * @param votingId Unique voting Id
     * @return bool
     */
    function isFinished(VotingStorage storage store, bytes32 votingId) internal view returns (bool) {
        require(store.votings[votingId].id == votingId, "UNKNOWN_VOTING");
        return now >= store.votings[votingId].stop;
    }

    /**
     * @dev Get the voting result
     * @param store Votings storage
     * @param votingId Unique voting Id
     * @param optionIndex Option index
     * @return uint256
     */
    function result(
        VotingStorage storage store,
        bytes32 votingId,
        uint256 optionIndex
    ) internal view returns (uint256) {
        require(store.votings[votingId].id == votingId, "UNKNOWN_VOTING");
        require(now >= store.votings[votingId].start, "VOTING_NOT_STARTED");

        uint256 votingResult = 0;

        for (uint256 i = 0; i < store.votings[votingId].participantsAddresses.length; i++) {

            if (store.votings[votingId].votes[store.votings[votingId].participantsAddresses[i]].option == optionIndex) {//&& store.votings[votingId].votes[store.votings[votingId].participantsAddresses[i]].participant != address(0
                
                votingResult += sqrt(
                    store
                        .votings[votingId]
                        .votes[store.votings[votingId].participantsAddresses[i]]
                        .count
                );
            }
        }

        return votingResult;
    }

    /**
     * @dev Get the number of voices left
     * @param store Votings storage
     * @param owner Participant address
     * @param credit Maximum number of voices available
     * @return uint256
     */
    function voicesLeft(
        VotingStorage storage store,
        address owner,
        uint256 credit
    ) internal view returns (uint256) {
        uint256 totalVoices = 0;

        for (uint256 i=0; i < store.votingsIds.length; i++) {

            if (store.votings[store.votingsIds[i]].participants[owner]) {
                
                totalVoices += store.votings[store.votingsIds[i]].votes[owner].count;
            }
        }

        return credit - totalVoices;
    }

    /**
     * @dev Returns unique hash that can be used as ID
     * @param store Unique hashes storage
     * @return bytes32 Unique hash
     */
    function getUniqueHash(VotingStorage storage store) private returns (bytes32) {
        uint256 blockNumber = block.number - 1;
        bytes32 uHash = keccak256(
            abi.encodePacked(
                store.lastHash,
                msg.sender,
                gasleft(),
                block.gaslimit,
                block.difficulty,
                msg.sig,
                blockhash(blockNumber)
            )
        );

        require(store.hashes[uHash] != true, "NOT_UNIQUE_HASH");
        store.hashes[uHash] = true;
        store.lastHash = uHash;        
        return uHash;
    }

    /**
     * @dev Returns a square root
     * @param x Source
     * @return square root from the x
     */
    function sqrt(uint256 x) private pure returns (uint256 y) {
        
        assembly {
            let z := div(add(x, 1), 2)
            y := x
            for { } lt(z, y) { } {
                y := z
                z := div(add(div(x, z), z), 2)
            }
        }

        return y;
    }
}
