pragma solidity 0.5.0;

import "../managers/VotingManager.sol";


/**
 * @title VotingManagerTest
 * @dev This contract represents testing logic for VotingManager
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract VotingManagerTest is VotingManager {

    /**
     * @dev VotingManagerTest constructor
     * @param creditValue A maximum number of available votes
     */
    constructor(
        uint256 creditValue
    ) public VotingManager(creditValue) {}

    /**
     * @dev Trying to call isRegistered function with zero owner parameter
     */
    function testIsRegisteredWithWrongAddress() public view returns (bool) {
        return votings.isRegistered(address(0), keccak256("bla-bla-bla"));
    }

    /**
     * @dev Testing MaxVotes error during making a vote
     * @param votingId Unique voting Id
     * @param option Voting option index
     * @param votes Number of votes to use
     */
    function testMaxVotesDuringTheVote(
        bytes32 votingId, 
        uint256 option, 
        uint256 votes
    ) public {
        votings.vote(votingId, msg.sender, option, votes, 0);
    }
}
