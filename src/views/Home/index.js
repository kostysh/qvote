import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Accordion, AccordionPanel, Grid, Box, Button, Text, Anchor, TextInput,
    Table, TableHeader, TableBody, TableRow, TableCell, RadioButton, Markdown } from 'grommet';

import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';

class Home extends PureComponent {

    state = {
        choices: {},
        voices: {}
    };

    constructor(props) {
        super(props);

        this.voicesRef = React.createRef();
    }

    componentDidMount = () => {
        this.props.messagesClean();

        if (!this.props.isMetamaskError) {
            
            this.props.fetchVotings();
        }
    };

    onVote = (voting, value) => {
        this.setState({
            choices: {
                [voting.id]: value
            },
            voices: {
                [voting.id]: parseInt(this.voicesRef.current.value)
            }
        });
    }

    onVoice = (voting, value) => {
        this.setState({
            voices: {
                [voting.id]: value
            }
        });
    }

    onSaveVoices = (votingId) => {
        const { choices, voices } = this.state;
        // console.log('>>>', choices[votingId], voices[votingId]);
        this.props.vote(votingId, choices[votingId], voices[votingId]);
    }

    onRegister = (votingId) => {
        // console.log('>>>', votingId);
        this.props.register(votingId);
    };

    render() {

        const { votings, isMetamaskError, error, isFetching, votingsMessages } = this.props;
        const { choices, voices } = this.state;

        return (
            <div>
                {isMetamaskError &&
                    <Box pad="large" align="center" background="light-1">
                        <Text color="status-error">It seems <Anchor href="https://metamask.io/">Metamask plugin</Anchor> is not 
                        installed OR you not logged-in into your account yet</Text>
                        <Anchor onClick={this.props.refreshConnection}>Click on this text to refresh</Anchor>
                    </Box>
                }
                {error &&
                    <Box pad="large" align="center" background="light-1">
                        <Text color="status-error">{error}</Text>
                        <Anchor onClick={this.props.invalidateError}>Click on this text to close this message</Anchor>
                    </Box>
                }
                {(votings.length === 0 && !isFetching) &&
                    <Box pad="large" align="center" background="light-1">
                        <Text color="status-warning">There no registered votings found</Text>
                        <Anchor onClick={this.props.fetchVotings}>Click on this text to refresh</Anchor>
                    </Box>
                }
                <Accordion>
                    {votings.map(voting => (
                        <AccordionPanel label={`${voting.name} (${voting.start} - ${voting.stop})${voting.started ? ' started' : (voting.finished ? 'finished' : '')}`} key={voting.id}>
                            {(!voting.accountRegistered && !voting.finished) && 
                                <Box pad="medium" background="light-2">
                                    <Button label="Register" onClick={() => this.onRegister(voting.id)} />
                                </Box>
                            }
                            <Box pad="medium" background="light-2">
                                <Table>
                                    <TableHeader>
                                        <TableRow>
                                            <TableCell scope="col" border="bottom">
                                                Option
                                            </TableCell>
                                            <TableCell scope="col" border="bottom">
                                                Voices
                                            </TableCell>
                                            <TableCell scope="col" border="bottom">
                                                Choice
                                            </TableCell>
                                        </TableRow>                                    
                                    </TableHeader>
                                    <TableBody>
                                        {voting.options.map((opt, index) => (
                                            <TableRow key={index}>
                                                <TableCell scope="row">
                                                    <strong>{opt}</strong>
                                                </TableCell>
                                                <TableCell scope="row">
                                                    {voting.votes[index]}
                                                </TableCell>                                                
                                                <TableCell scope="row">
                                                    <RadioButton
                                                        name={voting.id}
                                                        checked={choices[voting.id] === index}
                                                        label=''
                                                        value={index}
                                                        onChange={event => this.onVote(voting, parseInt(event.target.value))} 
                                                        disabled={!voting.started || voting.finished}
                                                    />
                                                </TableCell>                                                                                                
                                            </TableRow>
                                        ))}                                     
                                    </TableBody>
                                </Table>                                                       
                            </Box>
                            {voting.started && 
                                <Grid
                                    fill
                                    rows={['flex']}
                                    columns={['small', 'flex']}
                                    gap="none" 
                                    areas={[
                                        { name: 'left', start: [0, 0], end: [0, 0] },
                                        { name: 'right', start: [1, 0], end: [1, 0] },
                                    ]}>
                                    <Box gridArea="left" pad="medium" background="light-2">
                                        <TextInput 
                                            ref={this.voicesRef}
                                            type="number" 
                                            label="Voices" 
                                            value={voices[voting.id] || 1} 
                                            disabled={voting.finished}
                                            onChange={event => this.onVoice(voting, parseInt(event.target.value))} />
                                    </Box>
                                    <Box gridArea="right" pad="medium" background="light-2">
                                        <Button 
                                            label='Specify the number of votes and vote' 
                                            primary 
                                            disabled={voting.finished}
                                            onClick={() => this.onSaveVoices(voting.id)} />
                                    </Box>
                                </Grid>
                            }                                                    
                        </AccordionPanel>
                    ))}                
                </Accordion>
                {votingsMessages.map((msg, index) => (
                    <Box pad="small" align="center" background="light-2" key={index}>
                        <Text color="status-warning">
                            <Markdown color="status-error">{msg}</Markdown>
                        </Text>
                        <Anchor onClick={() => this.props.messageDismiss(index)}>Click on this text to close this message</Anchor>
                    </Box>
                ))}
            </div>            
        );
    }
}

function mapStateToProps(state) {

    return {
        isMetamaskError: selectors.accountsError(state),
        isFetching: !selectors.isVotingsFetching(state),
        error: selectors.votingsError(state),
        votings: selectors.votings(state),
        votingsMessages: selectors.votingsMessages(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        refreshConnection: () => dispatch(actions.accountsFetch()),
        invalidateError: () => dispatch(actions.votingsInvalidateError()),
        fetchVotings: () => dispatch(actions.votingsFetch()),
        messageDismiss: index => dispatch(actions.votingsMessageDismiss(index)),
        messagesClean: () => dispatch(actions.votingsMessagesClean()),
        register: votingId => dispatch(actions.registerAccount(votingId)),
        vote: (votingId, choice, voices) => dispatch(actions.saveVote(votingId, choice, voices))
    };
};

const connectedHome = connect(mapStateToProps, mapDispatchToProps)(Home);

export default connectedHome;

export const route = {
    path: '/',
    exact: true,
    label: 'Votings',
    component: connectedHome
};
