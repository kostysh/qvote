import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Box, Text, Button, TextInput, Anchor, Markdown } from 'grommet';
import * as Icons from 'grommet-icons';
import './Create.scss';

import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';

class Create extends Component {

    state = {
        name: '',
        start: '',
        stop: '',
        options: ['']
    };

    componentDidMount = () => {
        this.props.messagesClean();
        this.setState({
            name: '',
            start: '',
            stop: '',
            options: ['']
        });
    };

    addOption = () => {
        this.setState({
            options: [...this.state.options, ...['']]
        });
    };

    onRemoveOption = (index) => {
        let optionsOrig = this.state.options;
        optionsOrig.splice(index, 1);

        this.setState({
            options: optionsOrig
        });
    };

    createVoting = () => {
        
        if (!this.props.isMetamaskError) {

            const { name, start, stop, options } = this.state;
            this.props.createVoting({ name, start, stop, options });
            this.setState({
                name: '',
                start: '',
                stop: '',
                options: ['']
            });
        }        
    };

    render() {
        const { isMetamaskError, error, votingsMessages } = this.props;

        return (
            <div>
                {isMetamaskError &&
                    <Box pad="large" align="center" background="light-1">
                        <Text color="status-error">It seems <Anchor href="https://metamask.io/">Metamask plugin</Anchor> is not 
                        installed OR you not logged-in into your account yet</Text>
                        <Anchor onClick={this.props.refreshConnection}>Click on this text to refresh</Anchor>
                    </Box>
                }
                {error &&
                    <Box pad="large" align="center" background="light-1">
                        <Text color="status-error">{error}</Text>
                        <Anchor onClick={this.props.invalidateError}>Click on this text to close this message</Anchor>
                    </Box>
                }
                <Box pad="medium" background="light-2">                
                    <Box pad="small">
                        <Text margin={{bottom: 'small'}}>Voting name:</Text>
                        <TextInput
                            name="name"
                            placeholder="Voting name" 
                            maxLength="32"
                            onChange={event => this.setState({name: event.target.value})}
                            value={this.state.name}
                        />
                    </Box>
                    <Box pad="small">
                        <Text margin={{bottom: 'small'}}>Start date:</Text>
                        <TextInput 
                            placeholder="Start date" 
                            type="date" 
                            onChange={event => this.setState({start: event.target.value})} 
                            value={this.state.start}
                        />
                    </Box>
                    <Box pad="small">
                        <Text margin={{bottom: 'small'}}>Stop date:</Text>
                        <TextInput 
                            placeholder="Stop date" 
                            type="date" 
                            onChange={event => this.setState({stop: event.target.value})} 
                            value={this.state.stop}
                        />
                    </Box>
                    <Box pad="small">
                        <Text>Voting options:</Text>
                    </Box>
                    {this.state.options.map((option, index) => (
                        <Box direction="row" pad="small" key={index}>
                            <Text className="option-label">{`Option #${index}`}</Text>
                            <TextInput
                                placeholder="Option title" 
                                maxLength="32"
                                onChange={event => {
                                    let optionsOrig = this.state.options;
                                    optionsOrig[index] = event.target.value;
                                    this.setState({options: optionsOrig});
                                }} 
                                value={option}
                            />
                            {index !== 0 &&
                                <Button icon={<Icons.Close />} onClick={() => this.onRemoveOption(index)} />
                            }
                        </Box>
                    ))}
                    <Box direction="row" justify="between">
                        <Button icon={<Icons.Add />} label="Add option" onClick={this.addOption} />
                    </Box>
                    <Box direction="row" justify="between" margin={{ top: 'medium' }}>
                        <Button label="Create" primary onClick={this.createVoting} />
                    </Box>
                </Box>
                {votingsMessages.map((msg, index) => (
                    <Box pad="small" align="center" background="light-2" key={index}>
                        <Text color="status-warning">
                            <Markdown color="status-error">{msg}</Markdown>
                        </Text>
                        <Anchor onClick={() => this.props.messageDismiss(index)}>Click on this text to close this message</Anchor>
                    </Box>
                ))}
            </div>            
        );
    }
}

function mapStateToProps(state) {

    return {
        isMetamaskError: selectors.accountsError(state),
        error: selectors.votingsError(state),
        votingsMessages: selectors.votingsMessages(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        refreshConnection: () => dispatch(actions.accountsFetch()),
        invalidateError: () => dispatch(actions.votingsInvalidateError()),
        createVoting: voting => dispatch(actions.createVoting(voting)),
        messageDismiss: index => dispatch(actions.votingsMessageDismiss(index)),
        messagesClean: () => dispatch(actions.votingsMessagesClean())
    };
};

const connectedCreate = connect(mapStateToProps, mapDispatchToProps)(Create);

export default connectedCreate;

export const route = {
    path: '/create',
    exact: true,
    label: 'Create',
    component: connectedCreate
};
