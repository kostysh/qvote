import React, { Component } from 'react';
import { Route, NavLink, withRouter } from 'react-router-dom';
import { Grommet, Box, Text } from 'grommet';
import { Link as LinkIcon } from 'grommet-icons';
import config from '../../config';
import './App.scss';
import VoicesLeft from '../../containers/VoicesLeft';

import routes from '../../router';
import Online from '../../containers/Online';

const theme = {
    global: {
        colors: {
            brand: '#228BE6'
        },
        font: {
            family: 'Roboto',
            size: '14px',
            height: '20px',
        },
    },
};

const AppBar = (props) => (
    <Box tag = 'header'
        direction = 'row'
        align = 'center'
        justify = 'between'
        background = 'brand'
        pad = {{
            left: 'medium',
            right: 'small',
            vertical: 'medium'
        }}
        elevation = 'medium'
        style = {{
            zIndex: '1'
        }} 
        {...props}
    />    
);

class App extends Component {
    render() {
        return ( 
            <Grommet theme = {theme}>
                <AppBar>
                    <ul className="nav-menu">
                        {routes.map(route => (
                            <li key={route.path.toString()}>
                                <NavLink
                                    to={{ pathname: route.path, state: { prevPath: this.props.location.pathname } }}
                                    exact={route.exact} 
                                    className="nav-link"
                                    activeClassName="selected">{route.label}</NavLink>
                            </li>                            
                        ))}
                    </ul>
                    <Box direction = 'row'>
                        <LinkIcon size="medium" />
                        <Text>
                            <a 
                                className="link-etherscan" 
                                title="Open votings contract on the Etherscan" 
                                href={`https://rinkeby.etherscan.io/address/${config.contract}`}
                            >{config.contract}</a>
                        </Text>                        
                    </Box>
                    <VoicesLeft />
                    <Online />
                </AppBar>
                {routes.map((route, index) => (
                    <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={route.component} />
                ))}
            </Grommet>
        );
    }
};

export default withRouter(App);
