import { route as home } from '../views/Home';
import { route as create } from '../views/Create';

export default [
    home,
    create
];
