import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { classNames } from '../../utils/componets.helpers';
import './Online.scss';

import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';

const Indicator = (props = {}) => {
    return (
        <div 
            className={
                classNames({
                    'online-indicator': true,
                    'error': props.error,
                    'connecting': !props.connected && !props.error,
                    'connected': props.connected && !props.error
                })
            } 
            title={props.error ? props.error : props.connected ? 'Connected' : 'Connecting'}             
        />
    );
};

class Online extends PureComponent {

    componentDidMount() {

        if (!this.props.isConnected) {

            this.props.dispatch(actions.accountsFetch());
        }        
    }

    render() {
        const { isConnected, error } = this.props;

        return (
            <div onClick={this.props.refresh}>
                <Indicator error={error} connected={isConnected} />
            </div>
        );
    }
}

function mapStateToProps(state) {

    return {
        isConnected: !selectors.isAccountsFetching(state),
        error: selectors.accountsError(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        refresh: () => dispatch(actions.accountsFetch())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Online);
