import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Box, Text } from 'grommet';

import * as selectors from '../../store/selectors';

class VoicesLeft extends PureComponent {

    render() {
        const { voicesLeft } = this.props;        

        return (
            <Box pad="small" direction="row">
                <Text><strong>Voices left</strong>: {voicesLeft}</Text>
            </Box>
        );
    }
}

function mapStateToProps(state) {

    return {
        voicesLeft: selectors.voicesLeft(state)
    };
}

export default connect(mapStateToProps)(VoicesLeft);
