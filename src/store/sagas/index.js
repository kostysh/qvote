import { all } from 'redux-saga/effects';
import accounts from './accounts';
import votings from './votings';

export default function* rootSaga() {
    yield all([
        ...accounts,
        ...votings
    ]);
}
