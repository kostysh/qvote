import { put, fork, takeLatest } from 'redux-saga/effects';
import { fetchVotings, createVoting, registerAccount, vote, getVoicesLeft } from '../services/votings';

import * as actions from '../actions';

function* fetchAllVotings() {
    try {
        yield put(actions.votingsMessage('Fetching process is started'));
        const votings = yield fetchVotings();
        yield put(actions.votingsReceived(votings));
        yield put(actions.votingsMessagesClean());
        const voicesLeft = yield getVoicesLeft();
        yield put(actions.saveVotesLeft(voicesLeft));
    } catch(err) {
        yield put(actions.votingsError(err));
    }
}

function* createNewVoting(action) {
    try {
        yield put(actions.votingsMessage('Publication process is started. Please wait until the transaction will be sent and mined'));
        const receipt = yield createVoting(action.voting);
        yield put(actions.votingsMessage(`Voting has been created successfully, transasction on the Etherscan: [${String(receipt.transactionHash).slice(0, 8)}...](https://rinkeby.etherscan.io/tx/${receipt.transactionHash})`));
        yield put(actions.votingsFetch());
    } catch(err) {
        yield put(actions.votingsError(err));
    }
}

function* registerSelectedAccount(action) {
    try {
        yield put(actions.votingsMessage('Account registration process is started'));
        yield registerAccount(action.votingId);
        yield put(actions.votingsMessagesClean());
        yield put(actions.votingsMessage('Account has been registered successfully'));
        yield fetchAllVotings();        
    } catch(err) {
        yield put(actions.votingsError(err));
    }
}

function* saveVote(action) {
    try {
        yield put(actions.votingsMessage('Saving of your vote is started'));
        const receipt = yield vote(action.votingId, action.choice, action.voices);
        yield put(actions.votingsMessagesClean());
        yield put(actions.votingsMessage(`Your vote has been save successfully, transasction on the Etherscan: [${String(receipt.transactionHash).slice(0, 8)}...](https://rinkeby.etherscan.io/tx/${receipt.transactionHash})`));
        yield fetchAllVotings();
    } catch(err) {
        yield put(actions.votingsError(err));
    }
}

function* watchActions() {
    yield takeLatest(actions.CREATE_VOTING, createNewVoting);
    yield takeLatest(actions.VOTINGS_FETCH, fetchAllVotings);
    yield takeLatest(actions.VOTING_REGISTER_ACCOUNT, registerSelectedAccount);
    yield takeLatest(actions.VOTING_SAVE_VOTE, saveVote);
}

export default [
    fork(watchActions)
];
