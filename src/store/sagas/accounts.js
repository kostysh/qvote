import { put, fork, takeLatest } from 'redux-saga/effects';
import { setupWeb3 } from '../services/web3';

import * as actions from '../actions';

function* setup() {
    try {

        yield setupWeb3();
        yield put(actions.web3Initialized());
    } catch(err) {
        console.log(err)
        yield put(actions.accountsError(err));
    }
}

function* watchActions() {
    yield takeLatest(actions.WEB3_SETUP, setup);
}

export default [
    fork(watchActions)
];
