import {
    VOTINGS_FETCH,
    VOTINGS_RECEIVED,
    VOTINGS_MESSAGE,
    VOTINGS_MESSAGE_DISMISS,
    VOTINGS_MESSAGES_CLEAN,
    VOTINGS_ERROR,
    VOTINGS_INVALIDATE_ERROR,
    VOICES_LEFT
} from '../actions';

const initialState = {
    isFetching: false,
    voicesLeft: 0,
    records: [],
    messages: [],
    errorMessage: null
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {
        
        case VOTINGS_FETCH:
            return { 
                ...state,
                isFetching: true,
                errorMessage: null 
            };

        case VOTINGS_RECEIVED:
            return {
                ...state,
                isFetching: false, 
                records: action.votings, 
                errorMessage: null
            };

        case VOICES_LEFT:
            return {
                ...state,
                voicesLeft: action.votes
            };

        case VOTINGS_MESSAGE:

            if (action.message && !Array.isArray(action.message)) {
                action.message = [action.message];
            }

            return {
                ...state, 
                messages: [
                    ...state.messages,
                    ...action.message
                ]
            };
        
        case VOTINGS_MESSAGE_DISMISS:                        
            return { 
                ...state,
                messages: state.messages.filter((item, index) => (index !== (action.index !== undefined ? action.index : index)))
            };

        case VOTINGS_MESSAGES_CLEAN:
            return {
                ...state,
                messages: []
            };

        case VOTINGS_ERROR:
            return { 
                ...state, 
                errorMessage: action.error.message 
            };

        case VOTINGS_INVALIDATE_ERROR:
            return { 
                ...state, 
                errorMessage: null 
            };
        
        default:
            return state;
    }
};
