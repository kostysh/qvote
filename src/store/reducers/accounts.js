import {
    WEB3_INITIALIZED,
    WEB3_SETUP,
    WEB3_ACCOUNTS_ERROR,
    WEB3_ACCOUNTS_INVALIDATE_ERROR
} from '../actions';

const initialState = {
    initialized: false,
    isFetching: false,
    errorMessage: null
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {

        case WEB3_INITIALIZED:
            return {
                ...state,
                initialized: true
            };
        
        case WEB3_SETUP:
            return { 
                ...state,
                isFetching: true,
                errorMessage: null 
            };

        case WEB3_ACCOUNTS_ERROR:
            return { 
                ...state, 
                errorMessage: action.error.message 
            };

        case WEB3_ACCOUNTS_INVALIDATE_ERROR:
            return { 
                ...state, 
                errorMessage: null 
            };
        
        default:
            return state;
    }
};
