import { reduce as accounts } from './accounts';
import { reduce as votings } from './votings';

export default {
    accounts,
    votings
};
