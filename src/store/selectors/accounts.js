export const accountsError = state => state.accounts.errorMessage;
export const isAccountsFetching = state => state.accounts.isFetching && !!state.accounts.errorMessage;
export const isWeb3Initialized = state => state.accounts.initialized;
