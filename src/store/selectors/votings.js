export const votingsError = state => state.votings.errorMessage;
export const isVotingsFetching = state => state.votings.isFetching && !!state.votings.errorMessage;
export const votings = state => state.votings.records;
export const votingsMessages = state => state.votings.messages;
export const voicesLeft = state => state.votings.voicesLeft;
