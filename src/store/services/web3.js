import Web3 from 'web3';
import config from '../../config';
import { store } from '../index';
import { votingsFetch } from '../actions/votings';

let initialized = false;

const onAccountChangeActions = () => {
    store.dispatch(votingsFetch());
};

/**
 * Setup web3
 */
export const setupWeb3 = async () => {

    if (window.ethereum) {            
        await window.ethereum.enable();   
    } else {
        throw new Error('It seems Metamask plugin not installed');
    }

    if (!initialized) {
        
        window.web3 = new Web3(window.web3.currentProvider);
        const provider = window.ethereum || window.web3.currentProvider;

        provider.removeListener('accountsChanged', onAccountChangeActions);
        provider.removeListener('networkChanged', onAccountChangeActions);
        provider.on('accountsChanged', onAccountChangeActions);
        provider.on('networkChanged', onAccountChangeActions);

        if (Number(window.ethereum.networkVersion) !== config.network) {
            throw new Error('Wrong network version. Please connect to the Rinkeby');
        }
        
        initialized = true;
    }       
};

/**
 * Check is web3 is initialized
 */
export const isWeb3Initialized = () => initialized;
