import { isWeb3Initialized, setupWeb3 } from './web3';
import config from '../../config';
import Qvote from '../../abi/Qvote.json';
import { unixTimestampToYMD } from '../../utils/date.helpers';

let contract;

/**
 * Initialize contact object from config
 */
const getContract = () => {

    if (contract) {

        return contract;
    }

    contract = new window.web3.eth.Contract(Qvote.abi, config.contract);

    return contract;
};

/**
 * Check is account is registered on given voting
 * @param {String} votingId
 * @returns {Boolean}
 */
export const isAccountRegistered = async (votingId) => {

    if (!isWeb3Initialized()) {

        await setupWeb3();
    }

    const qvote = getContract();
    const registered = await qvote.methods.isRegistered(window.ethereum.selectedAddress, votingId).call({
        from: window.ethereum.selectedAddress
    });

    return registered;
};

/**
 * Check is voting is started
 * @param {String} votingId
 * @returns {Boolean}
 */
export const isVotingStarted = async (votingId) => {

    if (!isWeb3Initialized()) {

        await setupWeb3();
    }

    const qvote = getContract();
    const started = await qvote.methods.isStarted(votingId).call({
        from: window.ethereum.selectedAddress
    });

    return started;
};

/**
 * Check is voting is finished
 * @param {String} votingId
 * @returns {Boolean}
 */
export const isVotingFinished = async (votingId) => {

    if (!isWeb3Initialized()) {

        await setupWeb3();
    }

    const qvote = getContract();
    const started = await qvote.methods.isFinished(votingId).call({
        from: window.ethereum.selectedAddress
    });

    return started;
};

/**
 * Fetch votings list
 * @returns {Object[]}
 */
export const fetchVotings = async () => {

    if (!isWeb3Initialized()) {

        await setupWeb3();
    }

    const qvote = getContract();

    const votingsIds = await qvote.methods.votingsIds().call({
        from: window.ethereum.selectedAddress
    });

    let details;
    let votings = [];

    for (let votingId of votingsIds) {
        details = await qvote.methods.details(votingId).call({
            from: window.ethereum.selectedAddress
        });

        let votes = [];

        let [started, finished, registered] = await Promise.all([
            isVotingStarted(votingId),
            isVotingFinished(votingId),
            isAccountRegistered(votingId)
        ]);

        if (started) {

            for (let i=0; i < details.options.length; i++) {
                
                let result = await qvote.methods.result(votingId, i).call({
                    from: window.ethereum.selectedAddress
                });
                votes.push(result.toNumber());
            }
        } else {

            votes = Array(details.options.length).fill(0);
        }

        votings.push({
            id: votingId,
            name: window.web3.utils.toUtf8(details.name),
            start: unixTimestampToYMD(details.start.toNumber()),
            stop: unixTimestampToYMD(details.stop.toNumber()),
            options: details.options.map(o => window.web3.utils.toUtf8(o)),
            votes,
            started,
            finished,
            accountRegistered: registered
        });
    }

    console.log('Votings:', votings);

    return votings;
};

/**
 * Create new voting
 * @returns {Object} Trasaction receipt
 */
export const createVoting = async (voting) => {

    if (!isWeb3Initialized()) {

        await setupWeb3();
    }

    const name = window.web3.utils.asciiToHex(voting.name);
    const offset = new Date().getTimezoneOffset()*60*1000;
    const start = (new Date(voting.start).getTime() + offset) / 1000;
    const stop = (new Date(voting.stop).getTime() + offset) / 1000;
    const options = voting.options.map(opt => window.web3.utils.asciiToHex(opt));    
    const qvote = getContract();
    const gasPrice = await window.web3.eth.getGasPrice();

    return await new Promise((resolve, reject) => {
        qvote.methods
            .create(name, start.toString(), stop.toString(), options)
            .send({
                from: window.ethereum.selectedAddress,
                gasPrice
            })
            .on('error', reject)
            .on('receipt', receipt => {

                if (Number(receipt.status) === 0) {
    
                    return reject(new Error('Transaction unsuccessful'));
                }
    
                resolve(receipt);
            });
    });
};

/**
 * Register currently selected accont for voting
 * @param {String} votingId
 * @returns {Object} Trasaction receipt
 */
export const registerAccount = async (votingId) => {

    if (!isWeb3Initialized()) {

        await setupWeb3();
    }

    const qvote = getContract();
    const gasPrice = await window.web3.eth.getGasPrice();

    return await new Promise((resolve, reject) => {
        qvote.methods
            .register(votingId)
            .send({
                from: window.ethereum.selectedAddress,
                gasPrice
            })
            .on('error', reject)
            .on('receipt', receipt => {

                if (Number(receipt.status) === 0) {
    
                    return reject(new Error('Transaction unsuccessful'));
                }
    
                resolve(receipt);
            });
    });
};

/**
 * Save a vote
 * @param {String} votingId
 * @param {Number} choice
 * @param {Number} voices
 * @returns {Object} Trasaction receipt
 */
export const vote = async (votingId, choice, voices) => {

    if (!isWeb3Initialized()) {

        await setupWeb3();
    }

    const qvote = getContract();
    const gasPrice = await window.web3.eth.getGasPrice();

    return await new Promise((resolve, reject) => {
        qvote.methods
            .vote(votingId, choice.toString(), voices.toString())
            .send({
                from: window.ethereum.selectedAddress,
                gasPrice
            })
            .on('error', reject)
            .on('receipt', receipt => {

                if (Number(receipt.status) === 0) {
    
                    return reject(new Error('Transaction unsuccessful'));
                }
    
                resolve(receipt);
            });
    });
};

/**
 * Get votes veft
 * @returns {Number} Number of voices
 */
export const getVoicesLeft = async () => {
    
    if (!isWeb3Initialized()) {

        await setupWeb3();
    }

    const qvote = getContract();
    const voices = await qvote.methods.voicesLeft().call({
        from: window.ethereum.selectedAddress
    });

    return voices.toNumber();
};
