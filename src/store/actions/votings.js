import { reduxAction as action } from '../../utils/redux.helpers';

export const VOTINGS_FETCH = 'VOTINGS_FETCH';
export const VOTINGS_RECEIVED = 'VOTINGS_RECEIVED';
export const VOTINGS_ERROR = 'VOTINGS_ERROR';
export const VOTINGS_INVALIDATE_ERROR = 'VOTINGS_INVALIDATE_ERROR';
export const CREATE_VOTING = 'CREATE_VOTING';
export const VOTINGS_MESSAGE = 'VOTINGS_MESSAGE';
export const VOTINGS_MESSAGE_DISMISS = 'VOTINGS_MESSAGE_DISMISS';
export const VOTINGS_MESSAGES_CLEAN = 'VOTINGS_MESSAGES_CLEAN';
export const VOTING_REGISTER_ACCOUNT = 'VOTING_REGISTER_ACCOUNT';
export const VOTING_SAVE_VOTE = 'VOTING_SAVE_VOTE';
export const VOICES_LEFT = 'VOICES_LEFT';

export const votingsFetch = () => action(VOTINGS_FETCH);
export const votingsReceived = votings => action(VOTINGS_RECEIVED, { votings });
export const votingsError = error => action(VOTINGS_ERROR, { error });
export const votingsInvalidateError = () => action(VOTINGS_INVALIDATE_ERROR);
export const createVoting = voting => action(CREATE_VOTING, { voting });
export const votingsMessage = message => action(VOTINGS_MESSAGE, { message });
export const votingsMessageDismiss = index => action(VOTINGS_MESSAGE_DISMISS, { index });
export const votingsMessagesClean = () => action(VOTINGS_MESSAGES_CLEAN);
export const registerAccount = votingId => action(VOTING_REGISTER_ACCOUNT, { votingId });
export const saveVote = (votingId, choice, voices) => action(VOTING_SAVE_VOTE, { votingId, choice, voices });
export const saveVotesLeft = votes => action(VOICES_LEFT, { votes });
