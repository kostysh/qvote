import { reduxAction as action } from '../../utils/redux.helpers';

export const WEB3_INITIALIZED = 'WEB3_INITIALIZED';
export const WEB3_SETUP = 'WEB3_SETUP';
export const WEB3_ACCOUNTS_ERROR = 'WEB3_ACCOUNTS_ERROR';
export const WEB3_ACCOUNTS_INVALIDATE_ERROR = 'WEB3_ACCOUNTS_INVALIDATE_ERROR';

export const web3Initialized = () => action(WEB3_INITIALIZED);
export const accountsFetch = () => action(WEB3_SETUP);
export const accountsError = error => action(WEB3_ACCOUNTS_ERROR, { error });
export const accountsInvalidateError = () => action(WEB3_ACCOUNTS_INVALIDATE_ERROR);
