module.exports.unixTimestampToYMD = timestamp => {
    const offset = new Date().getTimezoneOffset()*60;
    return (new Date((timestamp - offset) * 1000)).toISOString().slice(0,10);
};
