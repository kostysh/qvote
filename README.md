# qvote

A smart contract that implements a [quadratic voting](https://en.wikipedia.org/wiki/Quadratic_voting) mechanism

## Author
- Kostiantyn Smyrnov <kostysh@gmail.com>

## Prerequisites
- node.js (version 10 up) 
- installed and configured Metamask plugin (Chrome or Firefox web browser)

## Setup 

```sh
npm i
```

## Demo contract instance (Rinkeby network)
- [0xa3Ed7BBE054140e98A572995362350167e471f60](https://rinkeby.etherscan.io/address/0xa3ed7bbe054140e98a572995362350167e471f60) 

## Dapp Demo
- [https://arweave.net/2qdFSDXP_1RYxfl2BZzGQXjEXqfqPpjfw7SRxZ5a-g8](https://arweave.net/2qdFSDXP_1RYxfl2BZzGQXjEXqfqPpjfw7SRxZ5a-g8)

## Smart contract API
- A brief description of the smart contract API is [here](./docs/api.md) 
- Detailed information about functions please see in the source codes.
- Smart contract rules are mostly placed [in the library](./contracts/libraries/VotingLib.sol)

## Common rules of the smart contract

- Votings can be created by any account in any quantity
- To perform operations that change the state of a smart contract, a user account must have a positive balance (each operation uses a small amount of gas) 
- Any account can participate in an unlimited number of votes
- The limit of votes that a single account can use is limited and is a constant that is set during the creation of a smart contract and cannot be changed (in the deployed version of the contract this number is 100)
- Each vote can have an unlimited number of choices
- The length of the name of the vote and the length of the name of each of the options is limited to 32 characters (made specifically to reduce the cost of operations with the contract)
- To set the date of the beginning and end of the voting, you must specify the month, day and year (the restriction applies only to the application interface, the smart contract itself takes values ​​in the unix timestamp, which allows you to specify any time)
- To vote, you must register an account. Unregistered accounts cannot vote
- You can register an account in the voting only before it starts, if the voting has begun, then the registration is closed
- Each account can vote in one separate vote only once
- During voting, you can specify the required number of voices, which can not exceed the maximum number of votes (see above) 
- After each vote, the remaining (available) number of votes is recalculated and displayed in the application header. Sometimes you will need to update the UI to see changes
- After the end date of the vote, new votes will not be accepted
- In accordance with the rules for counting votes, the total values for each option are the sum of the quadratic roots of all votes. For example, if during the voting process the user specified the number of votes "10", then 3 votes will be counted (the integer part from the square root of 10). The winner is the option to collect the maximum amount

## Dapp overview
- [RU](./docs/dapp.ru.md)

## Running Dapp
### Development mode

```sh
npm run dapp:start
```
Dapp will be available on address http://localhost:3000

### Production mode

```sh
npm run compile
npm run dapp:build
```
Compiled code of the Dapp will be in the folder `./build`  
You can run builded Dapp using `serve` package.  

```sh
npm install -g serve
serve -s build
```

Dapp will be available on address http://localhost:5000

## Testing

```sh
npm test
```

with coverage*:

```sh
npm run test:coverage
```

*Coverage not working for now due to this [issue](https://github.com/sc-forks/solidity-coverage/issues/316).  
Latest solidity syntax version is not fully supported by `solidity-coverage` module
